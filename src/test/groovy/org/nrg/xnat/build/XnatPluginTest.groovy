package org.nrg.xnat.build

import org.junit.Test
import org.gradle.testfixtures.ProjectBuilder
import org.gradle.api.Project
import static org.junit.Assert.*

class XnatPluginTest {
    @Test
    public void xnatPluginAddsXnatTaskToProject() {
        Project project = ProjectBuilder.builder().build()
        project.pluginManager.apply 'org.nrg.xnat.build'

        assertTrue(project.tasks.xnat instanceof XnatTask)
    }
}
