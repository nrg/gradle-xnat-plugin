package org.nrg.xnat.build

import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction
import org.nrg.xdat.tasks.GenerateJavaFile

/*
 * Need to handle these args:
 *
 * <arg value="-xdir"/>
        <arg value="${basedir}"/>
        <arg value="-javaDir"/>
        <arg value="${basedir}/projects/${xdat.project.name}/src/java"/>
        <arg value="-templateDir"/>
        <arg value="${basedir}/projects/${xdat.project.name}/src/base-templates/screens"/>
        <arg value="-project"/>
        <arg value="${xdat.project.name}"/>
        <arg value="-e"/>
        <arg value="ALL"/>
        <arg value="-skipXDAT"/>
        <arg value="true"/>
        <arg value="-displayDocs"/>
        <arg value="true"/>
        <arg value="-dbUrl"/>
        <arg value="${xdat.project.db.connection.string}"/>
        <arg value="-dbUsername"/>
        <arg value="${xdat.project.db.user}"/>
        <arg value="-dbPassword"/>
        <arg value="${xdat.project.db.password}"/>

 */
class XnatTask extends DefaultTask {
    @TaskAction
    def process() {
        def String[] args = ["-xdir", "~"]
        final GenerateJavaFile worker = new GenerateJavaFile(args);
        //
    }
}
