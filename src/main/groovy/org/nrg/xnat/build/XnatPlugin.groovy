package org.nrg.xnat.build

import org.gradle.api.Project
import org.gradle.api.Plugin

class XnatPlugin implements Plugin<Project> {
    void apply(Project target) {
        target.task('xnat', type: XnatTask)
    }
}
